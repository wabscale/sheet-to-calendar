
venv:
	virtualenv -p $(shell which python3) venv
	./venv/bin/pip install -r ./requirements.txt

clean:
	rm -rf venv  __pycache__
