# Convert spreadsheets to google calendar events

### Google API setup

You'll need to create an API project, enable sheets and calendar, then create client 
credentials.


This is the annoying part. You need to create a project in the google api dashboard 
[here](https://console.developers.google.com/apis/dashboard). View the project you 
created, and click the `Enable APIS and Services` button. Enable google spreadsheets 
and google calendar. Go back to your project view. Go to the credentials panel. You 
need to either create a oauth2 client id  (if it didnt automatically). Download those 
creadentials. It should be `credentials.json`. Put them at the root of this project. 
Now you can run main.py. It will open a concent screen in your browser. You'll need to 
click through all the screens to enable the api.


### Required spreadsheet format

This script assumes you are using this format. You must match the column titles.

```csv
date,msg
2020-10-30 14:00:00,This is a thing
```

### Converting a csv

To convert a csv of events.

```shell
python3 main.py --csv ./path/to/events.csv
```

### Converting a google spreadsheet

You can either point the script to a spreadsheet by pulling the sheet id out of the url, 
or just give the script the url. I personally much prefer to just let the script parse out 
the spreadsheet id.

```shell
python3 main.py '<spreadsheetid>'
python3 main.py 'https://docs.google.com/spreadsheets/d/<spreadsheetid>/edit'
```

### Options

By default the script assumes the timezone is America/New_York. Change this by specifying the --tz option.

```shell
python3 main.py --tz Europe/Amsterdam --csv ./path/to/events.csv
```

The events created will have a duration of 15 minutes by default. You can specify whatever duration you 
want (in minutes) with the --duration option.

```shell
python3 main.py --duration 30 --csv ./path/to/events.csv
```
