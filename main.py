#!/usr/bin/env python

from __future__ import print_function
import traceback
import argparse
import datetime
import os.path
import typing
import pickle
import json
import csv
from urllib.parse import urlparse
from dateutil.parser import parse as dateparse
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = [
    'https://www.googleapis.com/auth/calendar',
    'https://www.googleapis.com/auth/spreadsheets.readonly'
]


def load_creds():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return creds


def load_calendar_data(args) -> typing.List[typing.Dict[str, str]]:
    """
    Load calendar data from source.
    """
    calendar_data = []

    if args.csv:
        # src is csv, so load accordingly
        with open(args.src, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                calendar_data.append({
                    'start': dateparse(row['date']).isoformat(),
                    'end': (dateparse(row['date']) + datetime.timedelta(minutes=args.duration)).isoformat(),
                    'summary': row['msg'],
                })

    else:
        # If the src is the url to s spreadsheet, then we need
        # to parse out the spreadsheet id
        spreadsheetId = urlparse(args.src).path
        if spreadsheetId.endswith('/edit'):
            spreadsheetId = spreadsheetId.split('/')[-2]

        creds = load_creds()
        service = build('sheets', 'v4', credentials=creds)
        sheet = service.spreadsheets().values().get(
            spreadsheetId=spreadsheetId,
            range='A2:B100', # Assume all the values are in the top corner. Skip the title row.
        ).execute()

        for row in sheet.get('values', []):
            calendar_data.append({
                'start': dateparse(row[0]).isoformat(),
                'end': (dateparse(row[0]) + datetime.timedelta(minutes=args.duration)).isoformat(),
                'summary': row[1],
            })

    return calendar_data


def main(args):
    """Shows basic usage of the Google Calendar API.
    Prints the start and name of the next 10 events on the user's calendar.
    """

    creds = load_creds()
    service = build('calendar', 'v3', credentials=creds)
    calendar_data = load_calendar_data(args)

    for row in calendar_data:
        try:
            # Attempt to add each event
            service.events().insert(calendarId='primary', body={
                'start': {
                    'timeZone': args.tz,
                    'dateTime': row['start'],
                },
                'end': {
                    'timeZone': args.tz,
                    'dateTime': row['end'],
                },
                'summary': row['summary'],
                'description': row['summary'],
            }).execute()

            # Report success
            print('[+] Added event {start} - {summary}'.format(summary=row['summary'], start=row['start']))

        except Exception as e:
            # Print traceback of failure, then continue
            print(traceback.format_exc())
            print('[-] Failed to add event {start} - {summary}'.format(summary=row['summary'], start=row['start']))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert csv or spreadsheet into google calendar events')
    parser.add_argument('--csv', action='store_true', help='Interpret src param as a path to a csv file')
    parser.add_argument('--duration', type=int, default=15, help='Duration of events to be created (in minutes)')
    parser.add_argument('--tz', default='America/New_York', help='Timezone of the dates specified (default America/New_York)')
    parser.add_argument('src', help='source file or google spreadsheet id')
    args = parser.parse_args()

    load_creds()
    main(args)
